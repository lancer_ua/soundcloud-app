//
//  LoginViewController.m
//  Soundcloud app
//
//  Created by Pavel Smirnov on 4/1/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController () <UITextFieldDelegate>

@end

@implementation LoginViewController 

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
    [[self loginTextField] setDelegate:self];
    [[self passwordTextField] setDelegate:self];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)configureView
{
    UIImage *tempImage = [UIImage imageNamed:@"soundcloud_logo_black.png"];
    tempImage = [tempImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    [[self mainView] setBackgroundColor:[UIColor orangeColor]];
    
    [[self logoImage] setContentMode:UIViewContentModeScaleAspectFit];
    [[self logoImage] setTintColor:[UIColor whiteColor]];
    [[self logoImage] setBackgroundColor:[UIColor orangeColor]];
    [[self logoImage] setImage:tempImage];
    
    [[self textLabel] setTextColor:[UIColor whiteColor]];
    
    [[self passwordTextField] setSecureTextEntry:YES];
    
    [[self confirmButton] setBackgroundColor:[UIColor whiteColor]];
    [[self confirmButton] setTitle:@"OK" forState:UIControlStateNormal];
    [[self confirmButton] setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [[self confirmButton] setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
}

@end
